<%@ page language="java" %>
<%@ taglib uri="http://metawidget.org/html" prefix="mh"%>
<%@ taglib uri="http://metawidget.org/spring" prefix="m"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
		
		<body>
		<div id="content">
		<form:form method="post" action="new" modelAttribute="person">
			<m:metawidget path="person">
			<!-- hide id -->
			 <m:stub path="id"></m:stub>

			<!-- display the address options -->			 
			 	<m:stub path="address">
			 	<!-- hide person -->
			 	 <m:stub path="person"></m:stub>
				<table>
						<thead>
							<tr>
								<th class="column-half">Address</th>
								<th class="column-half">Sub Locality</th>
								<th class="column-half">Locality</th>
								<th class="column-half">State</th>
								<th class="column-half">Country</th>
								<th class="column-half">Postal Code</th>
								
								<th class="column-tiny">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${person.address}" var="personAddress">
								<tr>						
									<td class="column-half"><mh:metawidget value="personAddress.address" style="width: 100%" /></td>
									<td class="column-half"><mh:metawidget value="personAddress.subLocality" style="width: 100%" /></td>
									<td class="column-half"><mh:metawidget value="personAddress.locality" style="width: 100%" /></td>
									<td class="column-half"><mh:metawidget value="personAddress.state" style="width: 100%" /></td>
									<td class="column-half"><mh:metawidget value="personAddress.country" style="width: 100%" /></td>
									<td class="column-half"><mh:metawidget value="personAddress.postalCode" style="width: 100%" /></td>
								</tr>								
							</c:forEach>
						</tbody>
						</table>
						</m:stub>
						
						
			<!-- display the social options -->
			<!-- TODO As soon as I include the social child list, the Address fields are lost and Social fields are displayed twice -->
<%-- 			 	<m:stub path="social">
				<table>
						<thead>
							<tr>
								<th class="column-half">Social Media</th>
								<th class="column-half">Profile</th>
								
								<th class="column-tiny">&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${person.social}" var="social">
								<tr>						
									<jsp:useBean id="social" class="au.com.avantek.model.Social"/>
									<td class="column-half"><mh:metawidget value="social.socialMediaProvider" style="width: 100%" /></td>
									<td class="column-half"><mh:metawidget value="social.socialMediaProfile" style="width: 100%" /></td>
								</tr>								
							</c:forEach>
						</tbody>
					</table>
					</m:stub> --%>			



				<m:facet name="footer">
							<input type="submit" name="save" value="<spring:message code="save"/>" onclick="if ( !confirm( 'Are you sure your details are correct?' )) return false"/>
				</m:facet>
			</m:metawidget>
		</form:form>
		</div>
	</body>	

	<p>
		<a href="/">Home</a>
	</p>
