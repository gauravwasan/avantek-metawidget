<%@ page language="java" %>
<%@ taglib uri="http://metawidget.org/html" prefix="mh"%>
<%@ taglib uri="http://metawidget.org/spring" prefix="m"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
		
		<h4>Registered Users</h4>
		
			<table class="data-table">
			<thead>
				<tr>
					<th class="column-half">Name</th>
					<th class="column-half">Email</th>
					<th class="column-tiny">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
							<c:forEach items="${persons}" var="person">
								<tr>
									<td class="column-half">${person.name}</td>
									<td class="column-half">${person.email}</td>
								</tr>
						</c:forEach>
			</tbody>
		</table>

	<p>
		<a href="/">Home</a>
	</p>
	<p>
		<a href="/new">Add</a>
	</p>