package au.com.avantek.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Interceptor to measure the response time of every call from the metawidget JSPs
 * @author Gaurav
 *
 */
public class PerformanceInterceptor extends HandlerInterceptorAdapter 
{
	private static final String REQUEST_TIME = "requestTime";
	static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PerformanceInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {

		long requestTime = System.currentTimeMillis();
		request.setAttribute(REQUEST_TIME, requestTime);
		return super.preHandle(request, response, handler);
	}


	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

			String requestUri = request.getRequestURI();
			Long requestTime = (Long)request.getAttribute(REQUEST_TIME);
			long processingTime = (System.currentTimeMillis() - requestTime);
			
			LOGGER.debug("In PerformanceInterceptor.afterCompletion(), request {} was served in {} milliseconds.", requestUri, processingTime);
		}
	}
