package au.com.avantek;

import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import au.com.avantek.interceptor.PerformanceInterceptor;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
 
/**
 * The most important class for setting the environment.
 * A few points
 * 1) Database connection is through hikari.properties
 * 2) persistence.xml is there because there are exceptions at startup if hikari does not find it, else not needed
 * 3) Other beans are self explanatory
 * 
 * @author Gaurav
 *
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "au.com.avantek")
public class AppConfig extends WebMvcConfigurerAdapter {
  
    @Bean
    public DataSource hikariDataSource() {
    	HikariConfig config = new HikariConfig("/hikari.properties");
    	HikariDataSource ds = new HikariDataSource(config);
	    return ds;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(hikariDataSource());
		entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
		
		return entityManagerFactoryBean;
	}
	
  	@Bean
	public JpaTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}
  	
	    
	/* this loads the messages.properties */
	@Bean
	public MessageSource messageSource() {
	    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	    messageSource.setBasename("messages");
	    // Used 86400 (one day) to auto refresh the property file after one day
	    messageSource.setCacheSeconds(86400);
	    return messageSource;
	}  
	
	@Bean
	public PerformanceInterceptor performanceInterceptor() {
	    return new PerformanceInterceptor();
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	    registry.addInterceptor(performanceInterceptor());
	} 	
}