package au.com.avantek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.com.avantek.model.Person;

/**
 * Spring Data Jpa repository for Person entity
 * 
 * @author Gaurav
 *
 */
@Repository
@Transactional
public interface PersonRepository extends JpaRepository<Person, Integer> {
}

