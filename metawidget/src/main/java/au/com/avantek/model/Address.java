package au.com.avantek.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The Address model. Its in a one to many relationship with Person model.
 * @author Gaurav
 *
 */
@Entity
@Table(name="PERSON_ADDRESS")
public class Address {

	/**
	 * auto generated primary key 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="address_id")
    private int addressId;

  @Column(name = "address")
  private String address;

  @Column(name = "sub_locality")
  private String subLocality;

  @Column(name = "locality")
  private String locality;

  @Column(name = "state")
  private String state;

  @Column(name = "country")
  private String country;

  @Column(name = "postal_code")
  private String postalCode;


	@ManyToOne (fetch=FetchType.LAZY, cascade = {CascadeType.MERGE}) 
	@JoinColumn(name = "id")  
	private Person person; 
	

	public Address(String address, String subLocality, String locality, String state, String country, String postalCode) {
	  this.address = address;
	  this.subLocality = subLocality;
	  this.locality = locality;
	  this.state = state;
	  this.country = country;
	  this.postalCode = postalCode;
	}

	public Address()
	  {
	    
	  }
	
  public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}



  public String getAddress()
    {
      return address;
    }

  public void setAddress(String address)
    {
      this.address = address;
    }

  public String getSubLocality()
    {
      return subLocality;
    }

  public void setSubLocality(String subLocality)
    {
      this.subLocality = subLocality;
    }

  public String getLocality()
    {
      return locality;
    }

  public void setLocality(String locality)
    {
      this.locality = locality;
    }

  public String getState()
    {
      return state;
    }

  public void setState(String state)
    {
      this.state = state;
    }

  public String getCountry()
    {
      return country;
    }

  public void setCountry(String country)
    {
      this.country = country;
    }

  public String getPostalCode()
    {
      return postalCode;
    }

  public void setPostalCode(String postalCode)
    {
      this.postalCode = postalCode;
    }

  @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + addressId;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Address))
			return false;
		Address other = (Address) obj;
		if (addressId != other.addressId)
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		return true;
	}
}
