package au.com.avantek.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.NotEmpty;
import org.metawidget.inspector.annotation.UiComesAfter;
import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLabel;

/**
 * The main model class for Person. This class has one to many relationships with Address and Social models.
 * @author Gaurav
 *
 */
@Entity
@Table(name = "PERSON")
public class Person {

  public Person() {

  }

  /**
   * auto generated primary key 
   */
  @UiHidden
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @UiLabel("Section 1")
  
  @NotEmpty
  @Column(name = "name")
  private String name;

  @UiComesAfter("name")
  @Column(name = "email")
  private String email;

  @Column(name = "joining_date")
  private Timestamp joiningDate = new Timestamp(Calendar.getInstance().getTime().getTime());

  @UiLabel("Section 2")
  
  @UiComesAfter("email")
  @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "person")
  @Fetch(FetchMode.SUBSELECT)
  private List<Address> address;

  @UiLabel("Section 3")

  @UiComesAfter("address")
  // social details
  @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL, CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "person")
  @Fetch(FetchMode.SUBSELECT)
  private List<Social> social;

  public int getId()
    {
      return id;
    }

  public String getName()
    {
      return name;
    }

  public void setName(String name)
    {
      this.name = name;
    }

  public String getEmail()
    {
      return email;
    }

  public void setEmail(String email)
    {
      this.email = email;
    }

  public List<Address> getAddress()
    {
      return address;
    }

  public void addAddress(Address address)
    {
      if (null == this.address)
        {
          this.address = new ArrayList<>();
        }
      this.address.add(address);
    }

  public List<Social> getSocial()
    {
      return social;
    }

  public void addSocial(Social social)
    {

      if (null == this.social)
        {
          this.social = new ArrayList<>();
        }
      this.social.add(social);
    }

  @Override
  public int hashCode()
    {
      final int prime = 31;
      int result = 1;
      result = prime * result + id;
      result = prime * result + ((email == null) ? 0 : email.hashCode());
      return result;
    }

  @Override
  public boolean equals(Object obj)
    {
      if (this == obj) return true;
      if (obj == null) return false;
      if (!(obj instanceof Person)) return false;
      Person other = (Person) obj;
      if (id != other.id) return false;
      if (email == null)
        {
          if (other.email != null) return false;
        }
      else if (!email.equals(other.email)) return false;
      return true;
    }

  @Override
  public String toString()
    {
      return String.format("name: [%s] email: [%s] joining date: [%s].", name, email, joiningDate);
    }

}
