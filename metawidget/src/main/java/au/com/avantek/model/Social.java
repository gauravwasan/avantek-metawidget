package au.com.avantek.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The Social model that holds the user's social network provider and profile details. Its in a one to many relationship with Person model.
 * @author Gaurav
 *
 */
@Entity
@Table(name="PERSON_SOCIAL")
public class Social {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="social_id")
    private int socialId;
	
	@Column(name = "social_media_provider")
	private String socialMediaProvider;
	
	@Column(name = "social_media_profile")
	private String socialMediaProfile;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade = { CascadeType.MERGE } )  
	@JoinColumn(name = "id")  
	private Person person; 
	

	public Social(String socialMediaProvider, String socialMediaProfile) {
	  this.socialMediaProvider = socialMediaProvider;
	  this.socialMediaProfile = socialMediaProfile;
  }
	
	public Social()
	  {
	    
	  }

  public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}


	public int getSocialId() {
		return socialId;
	}

	public void setSocialId(int socialId) {
		this.socialId = socialId;
	}

	public String getSocialMediaProvider() {
		return socialMediaProvider;
	}

	public void setSocialMediaProvider(String socialMediaProvider) {
		this.socialMediaProvider = socialMediaProvider;
	}

	public String getSocialMediaProfile() {
		return socialMediaProfile;
	}

	public void setSocialMediaProfile(String socialMediaProfile) {
		this.socialMediaProfile = socialMediaProfile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + socialId;
		result = prime * result + ((socialMediaProfile == null) ? 0 : socialMediaProfile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Social))
			return false;
		Social other = (Social) obj;
		if (socialId != other.socialId)
			return false;
		if (socialMediaProfile == null) {
			if (other.socialMediaProfile != null)
				return false;
		} else if (!socialMediaProfile.equals(other.socialMediaProfile))
			return false;
		return true;
	}
	
  @Override
  public String toString()
    {
      return String.format("social media provider: [%s] social media profile: [%s].", socialMediaProvider, socialMediaProfile);
    }
	
}
