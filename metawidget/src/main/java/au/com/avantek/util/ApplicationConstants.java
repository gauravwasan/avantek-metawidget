package au.com.avantek.util;

/**
 * The constants used in the assignment application
 * @author Gaurav
 *
 */
public class ApplicationConstants {

  public static final String INDEX_JSP = "index.jsp";
  public static final String PERSON_JSP = "person.jsp";
  public static final String PERSONS_JSP = "persons.jsp";
  public static final String REDIRECT_ALL = "redirect:/all";
  public static final String MODEL_ATTRIBUTE_STATUS = "status";
  public static final String FORM_ERROR_MESSAGE = "Please enter valid values to proceed.";

}
