package au.com.avantek.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.avantek.model.Address;
import au.com.avantek.model.Person;
import au.com.avantek.model.Social;
import au.com.avantek.service.PersonService;
import au.com.avantek.util.ApplicationConstants;

/**
 * The Spring Rest Controller that handles the addition and viewing of the users
 * 
 * @author Gaurav
 * 
 */
@Controller
@RequestMapping("/")
@Transactional
public class PersonController {

  // Define the log object for this class
  private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

  @Autowired
  PersonService personService;

  @Autowired
  MessageSource messageSource;

  @RequestMapping("/")
  public String index()
    {
      return ApplicationConstants.INDEX_JSP;
    }

  @RequestMapping(value = { "/new" }, method = RequestMethod.GET)
  public String addPerson(HttpServletRequest servletRequest, HttpServletResponse response, ModelMap model)
    {
      Person person = new Person();
      person.addAddress(new Address("home address", "subLocality", "locality", "state", "country", "postalCode"));
      person.addAddress(new Address("office address", "subLocality", "locality", "state", "country", "postalCode"));

      person.addSocial(new Social("facebook", "profile"));
      person.addSocial(new Social("twitter", "profile"));

      model.addAttribute("person", person);
      /*
       * 
       * TODO remove the block, using it to try getting metawidget to render child elements in the JSP model.addAttribute("address", person.getAddress()); model.addAttribute("social",
       * person.getSocial());
       */

      return ApplicationConstants.PERSON_JSP;
    }

  // the user posts all the details details
  @RequestMapping(value = { "/new" }, method = RequestMethod.POST)
  public String addPerson(HttpServletRequest servletRequest, @Valid Person person, BindingResult result, ModelMap model)
    {

      if (result.hasErrors())
        {
          LOGGER.debug("in PersonController.addPerson(), form has errors, sending person back to fill in the details.");
          model.addAttribute(ApplicationConstants.MODEL_ATTRIBUTE_STATUS, ApplicationConstants.FORM_ERROR_MESSAGE);
          return ApplicationConstants.PERSON_JSP;
        }

        // else move on
        {
          // persist person
          LOGGER.debug("in PersonController.addPerson(), saving new person's detail");
          personService.save(person);
          // redirect the person to show the list of persons
          return ApplicationConstants.REDIRECT_ALL;
        }
    }

  @RequestMapping(value = "all", method = RequestMethod.GET)
  public String showAll(HttpServletRequest request, ModelMap model)
    {
      LOGGER.debug("in PersonController.showAll()");
      try
        {
          List<Person> persons = personService.findAll();
          model.addAttribute("persons", persons);
          return ApplicationConstants.PERSONS_JSP;
        } catch (Exception e)
        {
          LOGGER.debug("in PersonController.showAll(), threw exception {}", e.getMessage());
          return ApplicationConstants.INDEX_JSP;
        }
    }
}
