package au.com.avantek.service;

import java.util.List;

import au.com.avantek.model.Person;

/**
 * The service operation declaration
 * @author Gaurav
 *
 */
public interface PersonService {

  void save(Person person);
  List<Person> findAll();

}
