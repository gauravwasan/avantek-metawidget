package au.com.avantek.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.avantek.model.Person;
import au.com.avantek.repository.PersonRepository;

/**
 * The service operation implementation
 * @author Gaurav
 *
 */
@Service("personService")
@Transactional
public class PersonServiceImpl implements PersonService {

  // Define the log object for this class
  private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private PersonRepository personRepository;

  @Override
  public List<Person> findAll()
    {
      LOGGER.debug("in PersonServiceImpl.findAll()");
      return personRepository.findAll();
    }

  @Override
  public void save(Person person)
    {
      LOGGER.debug("in PersonServiceImpl.save()");
      personRepository.saveAndFlush(person);
    }
}
