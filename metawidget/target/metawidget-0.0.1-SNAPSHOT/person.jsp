<%@ page language="java" %>
<%@ taglib uri="http://metawidget.org/html" prefix="mh"%>
<%@ taglib uri="http://metawidget.org/spring" prefix="m"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
		
		<body>
		<div id="content">
		<form:form method="post" action="new" modelAttribute="person">
			<m:metawidget path="person">
			<!-- hide id -->
			 <m:stub path="id"></m:stub>
			
					<c:forEach items="${address}" var="_communication">
								<tr>
									<td class="column-half">${_communication.address}</td>
									<td class="column-half">${_communication.subLocality}</td>
								</tr>
						</c:forEach>
				
				<m:facet name="footer">
							<input type="submit" name="save" value="<spring:message code="save"/>" onclick="if ( !confirm( 'Are you sure your details are correct?' )) return false"/>
				</m:facet>
			</m:metawidget>
			
			

		</form:form>
		
							<c:forEach items="${social}" var="_social">
								<tr>
									<td class="column-half">${_social.socialMediaProvider}</td>
									<td class="column-half">${_social.socialMediaProfile}</td>
								</tr>
						</c:forEach>
		
		</div>
	</body>	

	<p>
		<a href="/">Home</a>
	</p>
