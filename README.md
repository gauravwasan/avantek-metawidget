# README #

A metawidget demo application for Avantek recruitment. 
Metawidget is a cool framework that takes the pojos and automatically creates User Interface widgets.
see metawidget.org

This is a spring boot application using JSP with metawidget, spring rest, spring data jpa and mysql. 
Demo is running on http://metawidget.cfapps.io/

To run the project, clone the branch and update the hikari.properties with your database details. 
Then launch the Application.java as a standalone java application.
